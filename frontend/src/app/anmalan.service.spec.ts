import { TestBed, inject } from '@angular/core/testing';

import { AnmalanService } from './anmalan.service';

describe('AnmalanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnmalanService]
    });
  });

  it('should be created', inject([AnmalanService], (service: AnmalanService) => {
    expect(service).toBeTruthy();
  }));
});
