import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray } from "@angular/forms";
import { AnmalanService, RegistrationPost, AgeGroup } from '../../anmalan.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {

  form: FormGroup;

  ageGroups: AgeGroup[];
  selectedAgeGroups = new Array<AgeGroup>();

  constructor(
    private fb: FormBuilder,
    private anmalan: AnmalanService,
    private router: Router,
  ) {
    var people = [fb.group({
      firstname: '',
      lastname: '',
      ageGroupId: '',
      accommodationId: [{value: '', disabled: true}],
    })];

    this.form = fb.group({
      people: fb.array(people),
      family: false,
      address: '',
      postcode: '',
      city: '',
      phone: '',
      email: '',
      other: ''
    })
  }

  get people(): FormArray {
    return this.form.get('people') as FormArray;
  }

  addPerson() {
    this.people.push(this.fb.group({
      firstname: '',
      lastname: '',
      ageGroupId: '',
      accommodationId: [{value: '', disabled: true}],      
    }));
  }

  removePerson(i: number) {
    this.people.removeAt(i);
  }

  ngOnInit() {
    this.anmalan.getAgeGroups().subscribe(result => this.ageGroups = result);
  }

  save() {
    var registration: RegistrationPost = this.form.value;
    this.anmalan.saveRegistration(registration).subscribe(result => {
      this.router.navigate(['confirmation', result.id]);
    });
  }

  onChangeAgeGroup(i: number, ageGroupId: string) {
    var ctrl = this.form.get(`people.${i}.accommodationId`);
    if (ageGroupId) {
      this.selectedAgeGroups[i] = this.ageGroups.find(ag => ag.id === ageGroupId);
      ctrl.enable();  
    } else {
      ctrl.disable();
    }
  }
}
