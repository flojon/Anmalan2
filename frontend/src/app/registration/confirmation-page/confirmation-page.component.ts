import { Component, OnInit } from "@angular/core";
import { AnmalanService, Registration } from "../../anmalan.service";
import { ActivatedRoute } from "@angular/router";

import { switchMap, map } from "rxjs/operators";

@Component({
  templateUrl: './confirmation-page.component.html',
  styleUrls: ['./confirmation-page.component.scss'],
})
export class ConfirmationPageComponent implements OnInit {

    registration: Registration = null;

    constructor(
        private anmalan: AnmalanService,
        private route: ActivatedRoute,
    ) {}

    ngOnInit() {
        this.route.paramMap.pipe(
            map(p => p.get('id')),
            switchMap(id => this.anmalan.getRegistration(id))
        )
        .subscribe(r => this.registration = r);
    }
}
