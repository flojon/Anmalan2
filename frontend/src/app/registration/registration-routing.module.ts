import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegistrationComponent } from './registration.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { ConfirmationPageComponent } from './confirmation-page/confirmation-page.component';

const routes: Routes = [
  {
    path: '',
    component: RegistrationComponent,
    children: [
      { path: '', component: RegistrationPageComponent, pathMatch: 'full' },
      { path: 'confirmation/:id', component: ConfirmationPageComponent }
    ]
  },
]

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class RegistrationRoutingModule { }
