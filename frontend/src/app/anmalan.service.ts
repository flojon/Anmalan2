import { Injectable, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

export interface RegistrationPost {
  id: string;
  family: boolean;
  people: PersonPost[];
  address: string;
  postcode: string;
  city: string;
  phone: string;
  email: string;
  other: string;
}

export interface PersonPost {
  id: string;
  firstname: string;
  lastname: string;
  ageGroupId: string;
  accommodationId: string;
}

export interface Registration {
  id: string;
  family: boolean;
  people: Person[];
  address: string;
  postcode: string;
  city: string;
  phone: string;
  email: string;
  other: string;
  totalPrice: number;
  createdAt: Date;
  fondTransaction: number;
}

export interface Person {
  id: string;
  firstname: string;
  lastname: string;
  ageGroup: {
    id: string;
    name: string;
  };
  accommodation: {
    id: string;
    title: string;
  }
}

export interface AgeGroup {
  id: string;
  name: string;
  sortOrder: number;
  accommodations: Accommodation[];
}

export interface Accommodation {
  title: string;
  description: string;
  limit: number | null;
  familyPrice: number | null;
  sortOrder: number;
}

@Injectable()
export class AnmalanService {

  baseUrl = '/api/';

  constructor(
    private http: HttpClient
  ) {
    if (isDevMode())
      this.baseUrl = 'http://localhost:5000/api/'
  }

  getAgeGroups(): Observable<AgeGroup[]> {
    return this.http
      .get<AgeGroup[]>(this.baseUrl+'agegroups');
  }

  getPeople(): Observable<Person[]> {
    return this.http
      .get<Person[]>(this.baseUrl+'people');
  }

  getRegistrations(): Observable<Registration[]> {
    return this.http
      .get<Registration[]>(this.baseUrl+'registrations');
  }

  getRegistration(id: string): Observable<Registration> {
    return this.http
      .get<Registration>(this.baseUrl+`registrations/${id}`);
  }

  saveRegistration(registration: RegistrationPost): Observable<RegistrationPost> {
    return this.http
      .post<RegistrationPost>(this.baseUrl+'registrations', registration);
  }
}
