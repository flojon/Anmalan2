import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from './app.component';

import { RegistrationModule } from './registration/registration.module';

import { AnmalanService } from './anmalan.service';
import { AdminModule } from './admin/admin.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RegistrationModule,
    AdminModule,
    AppRoutingModule,
  ],
  providers: [
    AnmalanService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
