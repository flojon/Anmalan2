import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminComponent } from "./admin.component";
import { RegistrationListComponent } from "./registration-list/registration-list.component";
import { PeopleListComponent } from "./people-list/people-list.component";

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      { path: '', component: RegistrationListComponent, pathMatch: 'full' },
      { path: 'people', component: PeopleListComponent },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class AdminRoutingModule {

}