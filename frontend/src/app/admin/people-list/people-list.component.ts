import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AnmalanService, Person } from '../../anmalan.service';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {
  people: Observable<Person[]>

  constructor(
    private anmalan: AnmalanService,
  ) { }

  ngOnInit() {
    this.people = this.anmalan.getPeople();
  }

}
