import { Component, OnInit } from '@angular/core';
import { AnmalanService, Registration } from '../../anmalan.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-registration-list',
  templateUrl: './registration-list.component.html',
  styleUrls: ['./registration-list.component.scss']
})
export class RegistrationListComponent implements OnInit {

  registrations: Observable<Registration[]>;

  constructor(
    private anmalan: AnmalanService,
  ) { }

  ngOnInit() {
    this.registrations = this.anmalan.getRegistrations();
  }

}
