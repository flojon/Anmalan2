using System.ComponentModel.DataAnnotations;
using KoalaSoft.MongoDb.Repository;
using MongoDB.Bson.Serialization.Attributes;

namespace KoalaSoft.Anmalan.Backend.Models
{
    public class AccommodationBase: Entity
    {
        [Required]
        public string Title { get; set; }
    }

    public class Accommodation: AccommodationBase
    {
        public string Description { get; set; }
        [BsonIgnoreIfNull]
        public int? Limit { get; set; }
        public int? Price { get; set; }
        public int SortOrder { get; set; }
    }
}
