using System;
using System.Collections.Generic;
using KoalaSoft.MongoDb.Repository;
using MongoDB.Bson.Serialization.Attributes;

namespace KoalaSoft.Anmalan.Backend.Models
{
    // public class FamilyAccommodation: AccommodationBase
    // {
    //     public int Price { get; set; }
    // }

    public class Registration: Entity
    {
        public bool Family { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Other { get; set; }
        public int TotalPrice { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public int FondTransaction { get; set; } // Robin Hood kassan
        public IEnumerable<Person> People { get; set; }

        // [BsonIgnoreIfDefault]
        // [BsonIgnoreIfNull]
        // public FamilyAccommodation FamilyAccommodation { get; set; }
    }
}
