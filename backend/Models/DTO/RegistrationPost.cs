using System;
using System.Collections.Generic;
using KoalaSoft.MongoDb.Repository;
using MongoDB.Bson.Serialization.Attributes;

namespace KoalaSoft.Anmalan.Backend.Models.DTO
{
    public class RegistrationPost
    {
        public bool Family { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Other { get; set; }
        public int FondTransaction { get; set; } // Robin Hood kassan
        public IEnumerable<PersonPost> People { get; set; }
    }
}
