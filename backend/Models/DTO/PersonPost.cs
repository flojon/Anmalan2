namespace KoalaSoft.Anmalan.Backend.Models.DTO
{
    public class PersonPost
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string AgeGroupId { get; set; }
        public string AccommodationId { get; set; }
    }
}
