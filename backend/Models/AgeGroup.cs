using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using KoalaSoft.MongoDb.Repository;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KoalaSoft.Anmalan.Backend.Models
{
    public class AgeGroupBase: Entity
    {
        [Required]
        public string Name { get; set; }
    }

    public class AgeGroup: AgeGroupBase
    {
        public int SortOrder { get; set; }
        public IEnumerable<Accommodation> Accommodations { get; set; }
    }
}
