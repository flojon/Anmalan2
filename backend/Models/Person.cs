namespace KoalaSoft.Anmalan.Backend.Models
{
    public class Person
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public AgeGroupBase AgeGroup { get; set; }
        public AccommodationBase Accommodation { get; set; }
        public int Price { get; set; }
    }
}
