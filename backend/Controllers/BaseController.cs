using System.Collections.Generic;
using System.Threading.Tasks;
using KoalaSoft.MongoDb.Repository;
using Microsoft.AspNetCore.Mvc;

namespace KoalaSoft.Anmalan.Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseController<T>: ControllerBase
    {
        protected IRepository<T> _repository { get; }
        protected string _entityName { get; }

        public BaseController(IRepository<T> repository, string entityName)
        {
            _repository = repository;
            _entityName = entityName;
        }

        [HttpGet]
        public virtual Task<IEnumerable<T>> Get()
        {
            return _repository.Find();
        }

        [HttpGet("{id}")]
        public virtual async Task<ActionResult<T>> Get(string id)
        {
            var entity = await _repository.Get(id);
            if (entity == null)
                return NotFound($"No {_entityName} found with the given id");

            return Ok(entity);
        }

        [HttpPost]
        public virtual async Task<T> Post(T entity)
        {
            await _repository.Add(entity);

            return entity;
        }

        [HttpPut("{id}")]
        public virtual async Task<ActionResult<T>> Put(string id, T entity)
        {
            await _repository.Update(id, entity);
            entity = await _repository.Get(id);
            if (entity == null)
                return NotFound($"No {_entityName} found with the given id");

            return Ok(entity);
        }

        [HttpDelete("{id}")]
        public virtual async Task<ActionResult<T>> Delete(string id)
        {
            var entity = await _repository.Get(id);
            if (entity == null)
                return NotFound($"No {_entityName} found with the given id");

            await _repository.Remove(id);
                
            return entity;
        }
    }
}
