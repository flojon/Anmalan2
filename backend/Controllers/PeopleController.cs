using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KoalaSoft.Anmalan.Backend.Models;
using KoalaSoft.Anmalan.Backend.Models.DTO;
using KoalaSoft.MongoDb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KoalaSoft.Anmalan.Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PeopleController: Controller
    {
        protected IRepository<Registration> _registrationRepository { get; }
        private IRepository<AgeGroup> _ageGroupRepository { get; }
        public ILogger<RegistrationsController> _logger { get; }

        public PeopleController(
            IRepository<Registration> registrationRepository
        )
        {
            this._registrationRepository = registrationRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Person>> GetAll()
        {
            var registrations = await _registrationRepository.Find(_ => true, x => x.Id);

            return registrations.SelectMany(r => r.People).OrderBy(p => p.Lastname).ThenBy(p => p.Firstname);
        }
    }
}
