using System.Collections.Generic;
using System.Threading.Tasks;
using KoalaSoft.Anmalan.Backend.Models;
using KoalaSoft.MongoDb.Repository;

namespace KoalaSoft.Anmalan.Backend.Controllers
{
    public class AccommodationsController: BaseController<Accommodation>
    {
        public AccommodationsController(IRepository<Accommodation> _repository)
            : base(_repository, "Accommodation")
        { }

        public override Task<IEnumerable<Accommodation>> Get()
        {
            return _repository.Find(_ => true, x => x.Title);
        }
    }
}
