using System.Collections.Generic;
using System.Threading.Tasks;
using KoalaSoft.Anmalan.Backend.Models;
using KoalaSoft.MongoDb.Repository;

namespace KoalaSoft.Anmalan.Backend.Controllers
{
    public class AgeGroupsController: BaseController<AgeGroup>
    {
        public AgeGroupsController(IRepository<AgeGroup> _repository)
            : base(_repository, "age group")
        { }

        public override Task<IEnumerable<AgeGroup>> Get()
        {
            return _repository.Find(_ => true, x => x.SortOrder);
        }
    }
}
