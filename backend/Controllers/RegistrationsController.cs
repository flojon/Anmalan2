using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KoalaSoft.Anmalan.Backend.Models;
using KoalaSoft.Anmalan.Backend.Models.DTO;
using KoalaSoft.MongoDb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KoalaSoft.Anmalan.Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RegistrationsController: Controller
    {
        protected IRepository<Registration> _registrationRepository { get; }
        private IRepository<AgeGroup> _ageGroupRepository { get; }
        public ILogger<RegistrationsController> _logger { get; }

        public RegistrationsController(
            IRepository<Registration> registrationRepository,
            IRepository<AgeGroup> ageGroupRepository,
            ILogger<RegistrationsController> logger
        )
        {
            this._registrationRepository = registrationRepository;
            this._ageGroupRepository = ageGroupRepository;
            this._logger = logger;
        }

        [HttpGet]
        public Task<IEnumerable<Registration>> GetAll()
        {
            return _registrationRepository.Find(_ => true, x => x.Id);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Registration>> Get(string id)
        {
            var registration = await _registrationRepository.FindOne(r => r.Id == id);
            if (registration == null)
                return NotFound("No registration found with the given id");

            return Ok(registration);
        }

        [HttpPost]
        public async Task<Registration> Post(RegistrationPost model)
        {
            var people = new List<Person>();
            var ageGroupIds = model.People.Select(p => p.AgeGroupId);
            var ageGroups = await _ageGroupRepository.Find(ag => ageGroupIds.Contains(ag.Id));
            var totalPrice = 0;

            foreach (var p in model.People)
            {
                var ageGroup = ageGroups
                    .FirstOrDefault(ag => ag.Id == p.AgeGroupId);

                var accommodation = ageGroup
                    .Accommodations
                    .FirstOrDefault(acc => acc.Id == p.AccommodationId);

                var person = new Person
                {
                    Firstname = p.Firstname,
                    Lastname = p.Lastname,
                    AgeGroup = new AgeGroupBase
                    {
                        Id = ageGroup.Id,
                        Name = ageGroup.Name,
                    },
                    Accommodation = new AccommodationBase
                    {
                        Id = accommodation.Id,
                        Title = accommodation.Title,
                    },
                    Price = accommodation.Price ?? 0,
                };

                totalPrice += accommodation.Price ?? 0;
                people.Add(person);
            }

            var registration = new Registration
            {
                Family = model.Family,
                Address = model.Address,
                Postcode = model.Postcode,
                City = model.City,
                Phone = model.Phone,
                Email = model.Email,
                Other = model.Other,
                FondTransaction = model.FondTransaction,
                People = people,
                TotalPrice = totalPrice,
            };

            await _registrationRepository.Add(registration);

            return registration;
        }
    }
}
