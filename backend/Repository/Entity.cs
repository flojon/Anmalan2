using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KoalaSoft.MongoDb.Repository
{
    public class Entity : IEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public virtual string Id { get; set; }
    }
}
