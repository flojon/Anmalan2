using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KoalaSoft.MongoDb.Repository
{
    public class Repository<T>: IRepository<T>
        where T: IEntity
    {
        private IMongoDatabase _database { get; }
        private IMongoCollection<T> _collection { get; }
        
        public Repository(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
            if (_database != null)
                _collection = _database.GetCollection<T>(getCollectionName());
        }

        private string getCollectionName()
        {
            return typeof(T).Name;
        }

        public async Task Add(T model)
        {
            await _collection.InsertOneAsync(model);
        }

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> filter = null, Expression<Func<T, object>> sortBy = null, bool sortDescending = false)
        {
            var query = filter == null ? _collection.Find(_ => true) : _collection.Find(filter);
            if (sortBy != null) {
                if (sortDescending) {
                    query = query.SortByDescending(sortBy);
                } else {
                    query = query.SortBy(sortBy);
                }
            }

            return await query.ToListAsync();
        }

        public async Task<T> Get(string id)
        {
            return await _collection.Find(g => g.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id)
        {
            await _collection.DeleteOneAsync(g => g.Id == id);
        }

        public async Task Update(string id, T model)
        {
            model.Id = id;
            await _collection.ReplaceOneAsync(g => g.Id == id, model);
        }

        public async Task<T> FindOne(System.Linq.Expressions.Expression<System.Func<T, bool>> filter)
        {
            return await _collection.Find(filter).FirstOrDefaultAsync();
        }
    }
}

