using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KoalaSoft.MongoDb.Repository
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> filter = null, Expression<Func<T, object>> sortBy = null, bool sortDescending = false);
        Task<T> FindOne(System.Linq.Expressions.Expression<System.Func<T, bool>> filter);
        Task<T> Get(string id);
        Task Add(T model);
        Task Update(string id, T model);
        Task Remove(string id);
    }    
}
