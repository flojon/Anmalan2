using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization.Conventions;
using System;

namespace KoalaSoft.MongoDb.Repository
{
    public static class Extensions
    {
        public static void AddRepository(
            this IServiceCollection services,
            Action<Settings> configureOptions
        )
        {
            services.Configure<Settings>(configureOptions);
            services.AddTransient(typeof(IRepository<>),typeof(Repository<>));

            var pack = new ConventionPack();
            pack.Add(new CamelCaseElementNameConvention());
            ConventionRegistry.Register("camel case", pack, t => true);
        }
    }
}
