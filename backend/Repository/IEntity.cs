namespace KoalaSoft.MongoDb.Repository
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}