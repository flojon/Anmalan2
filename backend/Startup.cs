﻿using System;
using KoalaSoft.MongoDb.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace KoalaSoft.Anmalan.Backend
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddRepository(options => {
                options.ConnectionString = Configuration.GetSection("MongoDB:ConnectionString").Value;
                options.Database = Configuration.GetSection("MongoDB:Database").Value;
            });
        }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseCors(builder => builder.WithOrigins("http://localhost:4200")
                                        .AllowAnyMethod()
                                        .WithHeaders(new []{"authorization",  "content-type"})
                );
            }

            var rewriteOptions = new RewriteOptions()
                .Add(context =>
                {
                    var path = context.HttpContext.Request.Path;
                    if (path.StartsWithSegments(new PathString("/api")))
                        return;

                    if (path.Value.EndsWith(".js", StringComparison.OrdinalIgnoreCase) ||
                        path.Value.EndsWith(".css", StringComparison.OrdinalIgnoreCase) ||
                        path.Value.EndsWith(".html", StringComparison.OrdinalIgnoreCase))
                        return;

                    // If it's not /api and not jS/CSS/HTML then send to Angular
                    context.HttpContext.Request.Path = new PathString("/index.html");                    

                    context.Result = RuleResult.SkipRemainingRules;
                });
            app.UseRewriter(rewriteOptions);

            app.UseStaticFiles();

            app.UseMvc();
        }
    }
}
