FROM node AS angular-build
RUN yarn global add @angular/cli

RUN mkdir /src
WORKDIR /src

ADD frontend/yarn.lock frontend/package.json ./
RUN yarn install --frozen-lockfile

ADD frontend/ ./
RUN ng build --prod

FROM microsoft/aspnetcore-build:2.1.300-preview1 AS aspnet-build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY backend/*.csproj ./
RUN dotnet restore

# copy everything else and build app
COPY backend/. ./

RUN dotnet publish -o out /p:PublishWithAspNetCoreTargetManifest="false"


FROM microsoft/aspnetcore:2.1.0-preview1 AS runtime
ENV ASPNETCORE_URLS http://+:80
WORKDIR /app
COPY --from=aspnet-build /app/out ./
COPY --from=angular-build /src/dist/ ./wwwroot/
EXPOSE 80
ENTRYPOINT ["dotnet", "anmalan-backend.dll"]
